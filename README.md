# Configuration

Create needed directories with

```sh
mkdir -p ./amule/conf
mkdir -p ./amule/incoming
```

# Usage

Just run the container with the following command lines:
```sh
docker run -p 4713:4713 -p 6883:6883 -p 6993:6993/udp \
    -v ./amule/conf:/home/amule/.aMule -v ./amule/incoming:/incoming firerockvillage/amule
```
